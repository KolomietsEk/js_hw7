//Чайник. Возможные методы: ввод инф. о чайнике (мощность, объем, кол-во воды), вкл./выкл., расчет времени закипания воды.

let Teapot = function() {
    this.get = function() {
        this.p = +prompt('Введите мощность чайника, Вт');
        this.v = +prompt('Введите объем чайника, Л');
        this.tn = +prompt('Введите начальную температуру воды, С');

        this.operation();
    }

    this.operation = function() {
        this.result = (4183*this.v*(100-this.tn))/this.p;
        
        this.show();
    }

    this.show = function() {
        alert('Время закипания чайника ' + this.result/60 + ' минут');
    }
}

let teapot = new Teapot();
teapot.get();